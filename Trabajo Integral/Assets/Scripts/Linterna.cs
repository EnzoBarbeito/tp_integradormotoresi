using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.UI;
public class Linterna : MonoBehaviour
{
    public GameObject Prendido;
    public GameObject Maquina_de_luz;
    public GameObject Apagado;
    public Light luz;
    public ControlJugador CJ;
    private bool EstaPrendido;
    // Start is called before the first frame update
    void Start()
    {
        Prendido.SetActive(false);
        Apagado.SetActive(true);
        EstaPrendido=false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)&&CJ.tienelintera==true)
        {
            if (EstaPrendido)
            {
                Prendido.SetActive(false);
                Apagado.SetActive(true);
                GestorAudio.instancia.ReproducirSonido("L_ON");
                CJ.Objetos[0].gameObject.SetActive(true);
                CJ.Objetos[1].gameObject.SetActive(false);

            }
            if (!EstaPrendido)
            {
                Prendido.SetActive(true);
                Apagado.SetActive(false);
                GestorAudio.instancia.ReproducirSonido("L_ON");
                CJ.Objetos[0].gameObject.SetActive(false);
                CJ.Objetos[1].gameObject.SetActive(true);

            }


            EstaPrendido = !EstaPrendido;
        }
        if(EstaPrendido&& Input.mouseScrollDelta.y>0)
        {
            luz.range+=(30000*Time.deltaTime);
            luz.innerSpotAngle += 500 * Time.deltaTime;
        }
        else if (EstaPrendido && Input.mouseScrollDelta.y < 0)
        {
            luz.range-=30000* Time.deltaTime;
            luz.innerSpotAngle -= 500 * Time.deltaTime;


        }
        
    }
}
