using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Puerta: MonoBehaviour
{
    public Transform PlayerCamera;
    [Header("MaxDistance you can open or close the door.")]
    public float MaxDistance = 5;

    private bool opened = false;
    private Animator anim;



    void Update()
    {
        //This will tell if the player press F on the Keyboard. P.S. You can change the key if you want.
        if (Input.GetMouseButton(0))
        {
            Pressed();
            //Delete if you dont want Text in the Console saying that You Press F.
        }
    }

    void Pressed()
    {
        //This will name the Raycasthit and came information of which object the raycast hit.
        RaycastHit doorhit;

        if (Physics.Raycast(PlayerCamera.transform.position, PlayerCamera.transform.forward, out doorhit, MaxDistance))
        {

            // if raycast hits, then it checks if it hit an object with the tag Door.
            if (doorhit.transform.tag == "Door")
            {

                anim = doorhit.transform.GetComponentInParent<Animator>();

                opened = !opened;

                anim.SetBool("Opened", !opened);
            }
        }
    }
}