using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C4 : MonoBehaviour
{
    public float delay = 3f;
    public float FuerzaEX = 10f;
    public float RadioEX = 20f;
    public GameObject[] pedazos_pared;
    public GameObject ParedRota;
    public GameObject ParedNormal;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Explosion", delay);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Explosion ()
    {
        ParedNormal.SetActive(false);
        ParedRota.SetActive(true);
        GestorAudio.instancia.ReproducirSonido("GAS_explosion");
        Collider[] col = Physics.OverlapSphere(transform.position, RadioEX);
        
        foreach(Collider n in col)
        {
            Rigidbody rb = n.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(FuerzaEX, transform.position, RadioEX, 1f, ForceMode.Impulse);
            }
        }
        //foreach (GameObject x in pedazos_pared)
        //{
        //    Rigidbody cola = x.GetComponent<Rigidbody>();
        //    cola.isKinematic = true;
        //}
        Destroy(gameObject);
    }
   
  
}
