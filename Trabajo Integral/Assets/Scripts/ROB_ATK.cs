using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ROB_ATK : MonoBehaviour
{
    public GameObject Jugador;
    public int rapidez;
    public AudioSource run;
    public int vida = 100;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        if (vida == 0)
        {
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            run.Stop();
        }
    }

}
