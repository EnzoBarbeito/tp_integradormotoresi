using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueHacha : MonoBehaviour
{
    public GameObject CajaRota;
    public GameObject ParedRota;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Caja")
        {
            GameObject box = GameObject.Find("Caja");
            Destroy(box);
            CajaRota.SetActive(true);
        }
        if (other.name == "ParedBR")
        {
            GameObject box = GameObject.Find("ParedBR");
            Destroy(box);
            ParedRota.SetActive(true);
            Rigidbody rb = ParedRota.GetComponent<Rigidbody>();
            rb.AddForce(-ParedRota.transform.forward * 500);
            rb.useGravity = true;
        }
        if (other.name == "f7")
        {
            GameObject fragmento = GameObject.Find("f7");
            Rigidbody rb = fragmento.GetComponent<Rigidbody>();
            rb.AddForce(-fragmento.transform.forward * 500);
            rb.useGravity = true;

        }
    }
   
}
