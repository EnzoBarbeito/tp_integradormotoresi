using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inqulinacion : MonoBehaviour
{
    // Start is called before the first frame update
    public float amt, slerpamt;
    Quaternion rotacion;
    void Start()
    {
        rotacion = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            Quaternion izquierda = Quaternion.Euler(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z + amt);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, izquierda, Time.deltaTime * slerpamt);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            Quaternion derecha = Quaternion.Euler(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z - amt);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, derecha, Time.deltaTime * slerpamt);
        }
        else
        {
            transform.localRotation = Quaternion.Slerp(transform.localRotation, rotacion, Time.deltaTime* slerpamt);
        }
    }
}
