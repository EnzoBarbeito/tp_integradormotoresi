using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EventosROBPGeneral : MonoBehaviour
{
    public GameObject ROB_Spawn;// ROB a mostrar/activar.
    public GameObject ROB_Destroy; // ROB a destruir.
    public AudioSource ROB_Audio; //AudioSource de ROB.
    public TMP_Text Subs;// Subtitulos
    public GameObject Platforma_Spawn; // Plataforma que dispara el trigger a Activar.
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(ROB_Destroy);
        ROB_Spawn.SetActive(true);
        ROB_Audio.Play();
        if (this.gameObject.name == "EventoROBPG2")
        {

            StartCoroutine(PG2());

        }
        if (this.gameObject.name == "EventoROBPG3")
        {
            StartCoroutine(PG3());

        }
        if (this.gameObject.name == "EventoROBPG4")
        {
            StartCoroutine(PG4());

        }
        if (this.gameObject.name == "EventoROBPG5")
        {
            StartCoroutine(PG5());

        }
        if (this.gameObject.name == "EventoROBPG6")
        {
            StartCoroutine(PG6());

        }
        if (this.gameObject.name == "EventoROBPG7")
        {
            StartCoroutine(PG7());

        }
        if (this.gameObject.name == "EventoROBPG8")
        {
            StartCoroutine(PG8());

        }
        if (this.gameObject.name == "EventoROBPG9")
        {
            StartCoroutine(PG9());

        }
        Platforma_Spawn.SetActive(true);
    }
    IEnumerator PG2()
    {
        Subs.text = "�Sabias que la poblaci�n de este pueblo alguna vez fue de 300 personas?";
        yield return new WaitForSeconds(6.5f);
        Subs.text = "";
        Destroy(gameObject);
        yield return null;
    }
    IEnumerator PG3()
    {
        Subs.text = "La poblaci�n de este pueblo ahora es de...0 personas.";
        yield return new WaitForSeconds(5);
        Subs.text = "";
        Destroy(gameObject);
        yield return null;
    }
    IEnumerator PG4()
    {
        Subs.text = "Cada ciudadano de este pueblo muri� de maneras muy feas.";
        yield return new WaitForSeconds(4.5f);
        Subs.text = "�Me pregunto por qu�?";
        yield return new WaitForSeconds(2f);
        Subs.text = "";
        Destroy(gameObject);
        yield return null;
    }
    IEnumerator PG5()
    {
        Subs.text = "CORRE, CORRE, CORRE.";
        yield return new WaitForSeconds(2.9f);
        Subs.text = "Siempre parecen decir eso en las pel�culas de terror.";
        yield return new WaitForSeconds(4.3f);
        Subs.text = "No s� porque.";
        yield return new WaitForSeconds(2.8f);
        Subs.text = "";
        Destroy(gameObject);
        yield return null;
    }
    IEnumerator PG6()
    {
        Subs.text = "�Sabias acerca de la Combustion Espontanea Humana?";
        yield return new WaitForSeconds(4);
        Subs.text = "Sucede mas de lo que piensas...";
        yield return new WaitForSeconds(3);
        Subs.text = "";
        Destroy(gameObject);
        yield return null;
    }
    IEnumerator PG7()
    {
        Subs.text = "Detesto estar solo.";
        yield return new WaitForSeconds(3);
        Subs.text = "";
        Destroy(gameObject);
        yield return null;
    }
    IEnumerator PG8()
    {
        Subs.text = "Los rumores dicen que si sabes donde buscar...";
        yield return new WaitForSeconds(3.6f);
        Subs.text = "puede que encuentres una sorpresa.";
        yield return new WaitForSeconds(3f);
        Subs.text = "He estado aqu� por 25 a�os y aun no la pude encontrar.";
        yield return new WaitForSeconds(6.4f);
        Subs.text = "";
        Destroy(gameObject);
        yield return null;
    }
    IEnumerator PG9()
    {
        Subs.text = "Gracias por pasar.";
        yield return new WaitForSeconds(2.5f);
        Subs.text = "Recuerda de no tenerle miedo a las cosas, aun cuando no tengan buena pinta.";
        yield return new WaitForSeconds(7.7f);
        Subs.text = "";

        Destroy(gameObject);
        yield return null;
    }

}
