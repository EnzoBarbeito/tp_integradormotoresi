using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoROB_Hola : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject ROB;
    public AudioSource rsound;
    public TMPro.TMP_Text subs;
    void Start()
    {
        ROB = GameObject.Find("RobotDinner_Fuera");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        rsound.Play();
        StartCoroutine(Subtitulos());
    }
    IEnumerator Subtitulos()
    {
        subs.text = "Hola soy R.O.B, tu robot amigo.";
        yield return new WaitForSeconds(3.2f);
        subs.text = "Estoy aqui para ayudarte.";
        yield return new WaitForSeconds(2f);
        GestorAudio.instancia.ReproducirSonido("VZ_something");
        subs.text = "Esto si es algo...";
        yield return new WaitForSeconds(1.9f);
        subs.text = "";
        Destroy(this.gameObject);


    }
}
