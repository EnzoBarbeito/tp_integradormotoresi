using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoFinal : MonoBehaviour
{
    public GameObject PlataformaTrigger;
    public GameObject PlataformaTrigger4;
    public ControlJugador script;
    public GameObject ROB;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.name == "EventoFinal1")
        {
            GestorAudio.instancia.ReproducirSonido("GAS_gun");
            StartCoroutine(Subtitulos1());
        }
        if (gameObject.name == "EventoFinal2")
        {
            ROB.SetActive(true);
            PlataformaTrigger.SetActive(true);
            Destroy(gameObject, 4f);
        }
        if (gameObject.name == "EventoFinal3")
        {
            ROB.SetActive(true);
            PlataformaTrigger.SetActive(true);
            PlataformaTrigger4.SetActive(true);
            Destroy(gameObject, 4f);
        }
        if (gameObject.name == "EventoFinal4")
        {
            PlataformaTrigger.SetActive(true);
            Destroy(gameObject, 4f);
        }
        if (gameObject.name == "EventoFinal5")
        {
            ROB.SetActive(true);
            PlataformaTrigger.SetActive(true);
            Destroy(gameObject, 4f);
        }
        if (gameObject.name == "EventoFinal6")
        {
            ROB.SetActive(true);
            Destroy(gameObject, 4f);
        }
    }
    IEnumerator Subtitulos1()
    {
        script.subs.text = ("Ahora que estamos solos podemos hablar.");
        yield return new WaitForSeconds(2f);
        script.subs.text = ("Pero descuida, lo har� rapido.");
        yield return new WaitForSeconds(2f);
        script.subs.text = ("En este cuarto puede encontrar el gas que necesitas, pero tambien te deje una pistola.");
        yield return new WaitForSeconds(6f);
        script.subs.text = ("Tomala.");
        yield return new WaitForSeconds(2f);
        script.subs.text = ("La vas a necesitar.");
        yield return new WaitForSeconds(2f);
        script.subs.text = ("R.O.B esta en modo de cazeria.");
        yield return new WaitForSeconds(2f);
        script.subs.text = ("");
        PlataformaTrigger.SetActive(true);
        Destroy(gameObject);

    }
}
