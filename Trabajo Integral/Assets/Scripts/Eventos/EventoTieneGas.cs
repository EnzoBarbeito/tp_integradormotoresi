using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoTieneGas : MonoBehaviour
{
    public GameObject[] Cuadros;
    public Material[] Lista_Materials;
    public Renderer[] rnd;
    public GameObject ROB;
    public GameObject PlataformaTrigger;
    void Start()
    {
        rnd[0].enabled = true;
        rnd[1].enabled = true;
        rnd[2].enabled = true;
        rnd[3].enabled = true;
        rnd[4].enabled = true;
        rnd[5].enabled = true;
        rnd[6].enabled = true;
        rnd[7].enabled = true;
        rnd[8].enabled = true;
        rnd[9].enabled = true;
        rnd[10].enabled = true;
        rnd[11].enabled = true;
        rnd[12].enabled = true;


    }

    // Update is called once per frame
    void Update()
    {
        
    }
   
    private void OnTriggerEnter(Collider other)
    {
        rnd[12].sharedMaterial = Lista_Materials[2];
         for (int i = 0; i < 12; i++)
         {
           rnd[i].sharedMaterial = Lista_Materials[1];
         }
        ROB.SetActive(true);
        PlataformaTrigger.SetActive(true);
        Destroy(this.gameObject);

    }

    
}
