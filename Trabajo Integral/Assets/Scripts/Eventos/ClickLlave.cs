using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickLlave : MonoBehaviour
{
    public ControlJugador script;
    public GameObject puertafinal;
    public Material mat;
    Renderer rend;
    // Start is called before the first frame update
    void Start()
    {
        rend = gameObject.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDown()
    {
        if (script.tienellave == false)
        {
            StartCoroutine(Subtitulos());
        }
        if (script.tienellave == true)
        {
            Animator anim = puertafinal.GetComponent<Animator>();
            anim.SetTrigger("tienellave");
            script.Llave.gameObject.SetActive(false);
            rend.sharedMaterial = mat;
            GestorAudio.instancia.ReproducirSonido("SF_EnterPAD");
        }
    }
    IEnumerator Subtitulos()
    {
        script.subs.text = ("Esta cerrado.");
        yield return new WaitForSeconds(2f);
        script.subs.text = (" ");

    }
}
