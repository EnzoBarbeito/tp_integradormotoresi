using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParedDestruible : MonoBehaviour
{
    public ControlJugador script;
    public GameObject C4_Pared;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnMouseDown()
    {
        if (script.tienec4== false || script.C4.activeInHierarchy==false)
        {
            StartCoroutine(Subtitulos());
        }
        else if (script.tienec4 == true && script.C4.activeInHierarchy)
        {
            script.tienec4 = false;
            script.Objetos[5].gameObject.SetActive(false);
            script.Objetos[3].gameObject.SetActive(true);
            script.C4.SetActive(false);
            C4_Pared.SetActive(true);
        }

    }
    IEnumerator Subtitulos()
    {
        script.subs.text = ("Si tuivera algun explosivo podria volar la pared.");
        yield return new WaitForSeconds(3f);
        script.subs.text = ("");
    }
}
