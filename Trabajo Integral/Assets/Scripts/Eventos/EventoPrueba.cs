using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogoEventos : MonoBehaviour
{
    // Start is called before the first frame update
    public TMPro.TMP_Text texto_subs;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (this.gameObject.name=="EventoCamion")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_truck");
            StartCoroutine(Subtitulos());
        }
        if (this.gameObject.name == "EventoRadio")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_radio");
            StartCoroutine(Radio());
        }
        if (this.gameObject.name== "EventoRuidoRadio")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_noise");
            StartCoroutine(Ruido());
        }
        if (this.gameObject.name == "EventoOff")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_off");
            StartCoroutine(OFF());
        }



    }
    IEnumerator Subtitulos()
    {
        texto_subs.text = ("Si tienen un camion probablemente tengan algo de gas.");
        yield return new WaitForSeconds(3.5f);
        texto_subs.text = ("O al menos podrian darme un avent�n.");
        yield return new WaitForSeconds(2.8f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator Radio()
    {
        texto_subs.text = ("La radio.");
        yield return new WaitForSeconds(1.5f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator Ruido()
    {
        texto_subs.text = ("�Que es ese ruido?");
        yield return new WaitForSeconds(1.5f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator OFF()
    {
        texto_subs.text = ("Algo no esta bien aqu�.");
        yield return new WaitForSeconds(2f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
}
