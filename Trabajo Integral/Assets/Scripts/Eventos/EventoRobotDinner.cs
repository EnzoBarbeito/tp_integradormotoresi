using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoRobotDinner : MonoBehaviour
{
    public GameObject Plat_Trigger;
    public TMPro.TMP_Text Subs;
    GameObject RobotFuera;
    GameObject RobotDentro;
    public AudioSource ROB_DENTRO;
    // Start is called before the first frame update
    void Start()
    {
        RobotFuera = GameObject.Find("RobotDinner_Fuera");
        RobotDentro = GameObject.Find("RobotDinner_Dentro");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        RobotFuera.SetActive(false);
        RobotDentro.SetActive(true);
        Plat_Trigger.SetActive(true);
        ROB_DENTRO.Play();
        StartCoroutine(Subtitulos());
    }
    IEnumerator Subtitulos()
    {
        Subs.text = ("La cocina es el lugar mas importante para el chef.");
        yield return new WaitForSeconds(4);
        Subs.text = ("Es por eso que tenemos que mantenerla limpia de agua, aceite...");
        yield return new WaitForSeconds(5.2f);
        Subs.text = ("y sangre.");
        yield return new WaitForSeconds(2f);
        Subs.text = ("");
        yield return null;
        Destroy(gameObject);
    }



}
