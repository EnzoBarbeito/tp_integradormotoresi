using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventosROBPG1 : MonoBehaviour
{
    public GameObject ROB_DinnerOUT; // ROB a destruir.
    public GameObject ROB_DinnerIN; // ROB a destruir.
    public GameObject ROB_DinnerIN2; // ROB a destruir.
    public AudioSource ROB_Audio; //AudioSource de ROB.
    public TMPro.TMP_Text Subs;// Subtitulos
    public GameObject Platforma_Spawn; // Plataforma que dispara el trigger a Activar.
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (ROB_DinnerOUT.activeInHierarchy == true)
        {
            Destroy(ROB_DinnerOUT);
        }
        if (ROB_DinnerIN.activeInHierarchy == true)
        {
            Destroy(ROB_DinnerIN);
        }
        if (ROB_DinnerIN2.activeInHierarchy == true)
        {
            Destroy(ROB_DinnerIN2);
        }
        ROB_Audio.Play();
        Platforma_Spawn.SetActive(true);
        if (this.gameObject.name == "EventoROBPG1")
        {
            StartCoroutine(Subtitulos());

        } 
    }
    IEnumerator Subtitulos()
    {
        Subs.text = "Bien, tienes el gas. ahora te quedar�s...verdad?";
        yield return new WaitForSeconds(5.6f);
        Subs.text = "";
        Destroy(gameObject);
        yield return null;
    }

}
