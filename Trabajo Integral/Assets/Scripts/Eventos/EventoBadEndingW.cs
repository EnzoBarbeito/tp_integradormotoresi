using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoBadEndingW : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject PlatTrigger;
    public TMPro.TMP_Text texto_subs;
    void Start()
    {
        PlatTrigger = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        GestorAudio.instancia.ReproducirSonido("VZ_foot");
        StartCoroutine(Subtitulos());
    }
    IEnumerator Subtitulos()
    {
        texto_subs.text = ("�Me pregunto cuanto me tomar� si sigo a pie?");
        yield return new WaitForSeconds(3f);
        texto_subs.text = ("");
        Destroy(PlatTrigger);
    }
}
