using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Evento_BADJS : MonoBehaviour
{
    GameObject PlatTrigger;
    public GameObject ROBATK;
    public GameObject ROBATK2;
    public TMPro.TMP_Text texto_subs;
    void Start()
    {
        PlatTrigger = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        GestorAudio.instancia.ReproducirSonido("VZ_badending");
        StartCoroutine(Subtitulos());

    }
    IEnumerator Subtitulos()
    {
        texto_subs.text = ("�Que carajo es eso?");
        yield return new WaitForSeconds(3f);
        texto_subs.text = ("");
        Destroy(PlatTrigger);
        ROBATK.SetActive(true);
    }
}
