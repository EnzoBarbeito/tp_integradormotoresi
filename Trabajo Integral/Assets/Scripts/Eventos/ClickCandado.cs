using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickCandado : MonoBehaviour
{
    public ControlJugador script;
    public GameObject Candado_roto;
    public GameObject puerta;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDown()
    {
        if (script.tienehacha == false|| script.Hacha.activeInHierarchy==false)
        {
            StartCoroutine(Subtitulos());
        }
        if (script.tienehacha == true && script.Hacha.activeInHierarchy)
        {
            Destroy(gameObject);
            Candado_roto.SetActive(true);
            Animator anim = puerta.GetComponent<Animator>();
            anim.SetTrigger("candadoroto");
        }
    }
    IEnumerator Subtitulos()
    {
        script.subs.text = ("Si tuviera algo filoso podria romper este candado.");
        yield return new WaitForSeconds(4f);
        script.subs.text = (" ");
    }
}
