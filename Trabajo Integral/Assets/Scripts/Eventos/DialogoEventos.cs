using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoPrueba : MonoBehaviour
{
    // Start is called before the first frame update
    public TMPro.TMP_Text texto_subs;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (this.gameObject.name=="EventoCamion")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_truck");
            StartCoroutine(Subtitulos());
        }
        if (this.gameObject.name == "EventoRadio")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_radio");
            StartCoroutine(Radio());
        }
        if (this.gameObject.name== "EventoRuidoRadio")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_noise");
            StartCoroutine(Ruido());
        }
        if (this.gameObject.name == "EventoOff")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_off");
            StartCoroutine(OFF());
        }
        if (this.gameObject.name == "EventoCuadroPost")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_kind");
            StartCoroutine(SubtitulosPostCuadro());
        }
        if (this.gameObject.name == "EventoAbierto")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_open");
            StartCoroutine(SubtitulosOpen());
        }
        if (this.gameObject.name == "EventoTow")
        {
            GestorAudio.instancia.ReproducirSonido("VZ_tow");
            StartCoroutine(SubtitulosTow());
        }
        



    }
    IEnumerator Subtitulos()
    {
        texto_subs.text = ("Si tienen un camion probablemente tengan algo de gas.");
        yield return new WaitForSeconds(3.5f);
        texto_subs.text = ("O al menos podrian darme un avent�n.");
        yield return new WaitForSeconds(2.8f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator Radio()
    {
        texto_subs.text = ("La radio.");
        yield return new WaitForSeconds(1.5f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator Ruido()
    {
        texto_subs.text = ("�Que es ese ruido?");
        yield return new WaitForSeconds(1.5f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator OFF()
    {
        texto_subs.text = ("Algo no esta bien aqu�.");
        yield return new WaitForSeconds(2f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator SubtitulosPostCuadro()
    {
        texto_subs.text = ("�Que clase de lugar es este?");
        yield return new WaitForSeconds(1.8f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator SubtitulosOpen()
    {
        texto_subs.text = ("Raro, la puerta estar abierta.");
        yield return new WaitForSeconds(2.5f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
    IEnumerator SubtitulosTow()
    {
        texto_subs.text = ("Oh viejo, porque no llam� a la remolcadora?");
        yield return new WaitForSeconds(3f);
        texto_subs.text = ("");
        Destroy(this.gameObject);
    }
}
