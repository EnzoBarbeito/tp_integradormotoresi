using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EventoEasterEgg : MonoBehaviour
{
    public TMP_Text Subs;
    public AudioSource ROB;
    public TMP_Text Victoria;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        ROB.Play();
        StartCoroutine(Subitutlos());
    }
    IEnumerator Subitutlos()
    {
        Subs.text = ("Se ve que no eres un jugador normal.");
        yield return new WaitForSeconds(3);
        Subs.text = ("Respeto eso.");
        yield return new WaitForSeconds(2.5f);
        Subs.text = ("Gracias por jugar mi juego.");
        yield return new WaitForSeconds(3f);
        Subs.text = ("");
        GestorAudio.instancia.ReproducirSonido("Excelente");
        Victoria.gameObject.SetActive(true);
        Victoria.text = ("EXCELENTE, eres un gran jugador!");
        yield return new WaitForSeconds(6f);
        GestorAudio.instancia.ReproducirSonido("VZ_thingas");
        Subs.text = ("Las cosas que hago por gas.");
        yield return new WaitForSeconds(3f);
        Subs.text = ("");
        Destroy(this.gameObject);
        yield return null;
    }
}
