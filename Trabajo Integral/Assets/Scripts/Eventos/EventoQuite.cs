using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoQuite : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject PlatTrigger;
    public TMPro.TMP_Text texto_subs;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        GestorAudio.instancia.ReproducirSonido("VZ_quite");
        StartCoroutine(Subtitulos());
    }
    IEnumerator Subtitulos()
    {
        texto_subs.text = ("Toda la casa para estar en medio de la nada.");
        yield return new WaitForSeconds(3.5f);
        texto_subs.text = ("");
        Destroy(PlatTrigger);
    }
}
