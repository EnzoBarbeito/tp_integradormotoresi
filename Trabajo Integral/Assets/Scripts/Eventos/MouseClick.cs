using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClick : MonoBehaviour
{
    public ControlJugador script;
    public GameObject Barra;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDown()
    {
        if (script.tieneBateria == false)
        {
            StartCoroutine(Subtitulos());
        }
        if (script.tieneBateria==true&&script.Bateria.activeInHierarchy==true)
        {
            script.Bateria.SetActive(false);
            script.BateriaDisplay.SetActive(true);
            script.Objetos[6].gameObject.SetActive(false);
            script.Objetos[3].gameObject.SetActive(false);
            Animator anim = Barra.GetComponent<Animator>();
            anim.SetTrigger("pusobateria");
        }
    }
    IEnumerator Subtitulos ()
    {
        script.subs.text = ("La barrera, parece que le falta la bateria para abrir.");
        yield return new WaitForSeconds(4f);
        script.subs.text = ("");
    }
}
