using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class EventoRobotDinner2 : MonoBehaviour
{
    public GameObject RobotDentro;
    public GameObject Robot_Delete;
    public TMP_Text subs;
    public AudioSource rob_audio;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        RobotDentro.SetActive(true);
        Robot_Delete.SetActive(false);
        rob_audio.Play();
        StartCoroutine(Subtitutlos());
    }
    IEnumerator Subtitutlos()
    {
        subs.text = "Recuerda, si necesitas algo, hazmelo saber.";
        yield return new WaitForSeconds(4.6f);
        subs.text = "";
        Destroy(this.gameObject);
        yield return null;
    }
}
