using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacha : MonoBehaviour
{
    public GameObject axe;
    public float tiempo_cooldown=1f;
    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Atacar();
        }
    }
    public void Atacar()
    {
        Animator annie = axe.GetComponent<Animator>();
        annie.SetTrigger("Ataque");
        StartCoroutine(CoolDown());
    }
    IEnumerator CoolDown()
    {
        yield return new WaitForSeconds(tiempo_cooldown);
    }
}
