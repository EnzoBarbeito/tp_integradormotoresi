using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class ControlJugador : MonoBehaviour
{
    [Header("Basico")]
    public float rapidezDesplazamiento = 10.0f;
    public Camera CamaraFPS;
    public GameObject Sostenedor;

    [Header("HUD")]
    public Image[] EstadosJugador; //0=Parado, 1=Caminando, 2 =Corriendo, 3=Agachado.
    public Image[] Objetos; // 0=LinternaOFF, 1=LinternaON, 2=Gas
    public TMPro.TMP_Text ContadorEaster;
    public TMPro.TMP_Text objetivo;
    public TMPro.TMP_Text subs;
    public TMPro.TMP_Text Victoria;
    public TMPro.TMP_Text Derrota;
    public Image Llave;
    int contadorBotones;

    [Header("Eventos")]
    public GameObject Nota;
    public GameObject Lintera;
    public GameObject BidonJugador;
    public GameObject Hacha;
    public GameObject C4;
    public GameObject CuadroSecreto;
    public GameObject Bateria;
    public GameObject BateriaDisplay;
    public GameObject BateriaPickUp;
    
    public ParticleSystem flashpistola;
    public GameObject impacto;
    public bool tienelintera = false;
    public bool tienehacha = false;
    bool tienegas = false;
    public bool tienec4 = false;
    public bool tieneBateria = false;
    public bool tienellave = false;
    AudioSource radio;
    public AudioSource handgun;
    private Animator anim;
    bool abierto = true;
    public AudioSource ROB_ATK;
    public GameObject TriggerGas;
    public GameObject portal;
    public GameObject PostCuadro;
    int contadorascensor=0;
    public ROB_ATK scriptatk;

    [Header("Pausa")]
    public GameObject Vi�eta;
    bool pausa = false;
    public TMPro.TMP_Text GAS_pausa;
    public TMPro.TMP_Text Pausa;
    public TMPro.TMP_Text Objetivo_Pausa;
    public TMPro.TMP_Text P;

    [Header("Maquina de Numeros")]
    public TMPro.TMP_Text DisplayD1;
    public TMPro.TMP_Text DisplayD2;
    public TMPro.TMP_Text DisplayD3;
    public GameObject foco1;
    public GameObject foco2;
    Renderer rndf1;
    Renderer rndf2;
    public Material luzverde;
    public bool check1;
    public bool check2;
    public GameObject PuertaLab;
    public GameObject PuertaLabAbierta;
    public float velocidad;

    [Header("Maquina de Colores")]
    public GameObject NotaGuia;
    public Material[] Colores;
    public Renderer[] ColRender;
    int dg1, dg2, dg3, dg4, dg5, dg6;
    public GameObject PuertaLUZ;
    public GameObject Ascensor;

    [Header("Pistola")]
    public GameObject Pistola;
    public GameObject PistolaDisplay;
    public bool tienepistola = false;
    public int balas=10;

    void Start()
    {
        contadorBotones=0;
        Cursor.lockState = CursorLockMode.Locked;
        GestorAudio.instancia.ReproducirSonido("prueba");
        EstadosJugador[0].gameObject.SetActive(true);
        EstadosJugador[1].gameObject.SetActive(false);
        EstadosJugador[2].gameObject.SetActive(false);
        EstadosJugador[3].gameObject.SetActive(false);
        Objetos[3].gameObject.SetActive(true);
        Objetos[0].gameObject.SetActive(false);
        GestorAudio.instancia.ReproducirSonido("SN_viento");
        GestorAudio.instancia.ReproducirSonido("VZ_stranded");
        StartCoroutine(SubtitulosStart());
        rndf1 = foco1.GetComponent<Renderer>();
        rndf1.enabled = true;
        rndf2 = foco2.GetComponent<Renderer>();
        rndf2.enabled = true;
        check1 = false;
        check2 = false;

    }

    void Update()
    {
        
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        if (BidonJugador.activeInHierarchy==true)
        {
            Objetos[0].gameObject.SetActive(false);
            Objetos[1].gameObject.SetActive(false);
            Objetos[2].gameObject.SetActive(true);

        }
        if (Input.GetKeyDown(KeyCode.Alpha3)&& tienec4==true)
        {
            if (Lintera.activeInHierarchy == true)
            {
                Lintera.SetActive(false);
            }
            if (Hacha.activeInHierarchy == true)
            {
                Hacha.SetActive(false);
            }
            C4.SetActive(true);
            Objetos[0].gameObject.SetActive(false);
            Objetos[1].gameObject.SetActive(false);
            Objetos[2].gameObject.SetActive(false);
            Objetos[3].gameObject.SetActive(false);
            Objetos[4].gameObject.SetActive(false);
            Objetos[5].gameObject.SetActive(true);
            Objetos[6].gameObject.SetActive(false);
            Objetos[7].gameObject.SetActive(false);



        }
        if (C4.activeInHierarchy)
        {
            Objetos[0].gameObject.SetActive(false);
            Objetos[1].gameObject.SetActive(false);
            Objetos[2].gameObject.SetActive(false);
            Objetos[3].gameObject.SetActive(false);
            Objetos[4].gameObject.SetActive(false);
            Objetos[5].gameObject.SetActive(true);
            Objetos[6].gameObject.SetActive(false);
            Objetos[7].gameObject.SetActive(false);


        }
        if (Bateria.activeInHierarchy)
        {
            Objetos[0].gameObject.SetActive(false);
            Objetos[1].gameObject.SetActive(false);
            Objetos[2].gameObject.SetActive(false);
            Objetos[3].gameObject.SetActive(false);
            Objetos[4].gameObject.SetActive(false);
            Objetos[5].gameObject.SetActive(false);
            Objetos[6].gameObject.SetActive(true);
            Objetos[7].gameObject.SetActive(false);


        }
        if (Pistola.activeInHierarchy)
        {
            Objetos[0].gameObject.SetActive(false);
            Objetos[1].gameObject.SetActive(false);
            Objetos[2].gameObject.SetActive(false);
            Objetos[3].gameObject.SetActive(false);
            Objetos[4].gameObject.SetActive(false);
            Objetos[5].gameObject.SetActive(false);
            Objetos[6].gameObject.SetActive(false);
            Objetos[7].gameObject.SetActive(true);

        }
        if(Input.GetKeyDown(KeyCode.Alpha6)&& tienepistola == true)
        {
            if (Lintera.activeInHierarchy == true)
            {
                Lintera.SetActive(false);
            }
            if (Hacha.activeInHierarchy == true)
            {
                Hacha.SetActive(false);
            }
            if (C4.activeInHierarchy == true)
            {
                C4.SetActive(false);
            }
            if (Bateria.activeInHierarchy == true)
            {
                Bateria.SetActive(false);
            }
            Pistola.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5) && tieneBateria == true)
        {
            if (Lintera.activeInHierarchy == true)
            {
                Lintera.SetActive(false);
            }
            if (Hacha.activeInHierarchy == true)
            {
                Hacha.SetActive(false);
            }
            if (C4.activeInHierarchy == true)
            {
                C4.SetActive(false);
            }
            Bateria.SetActive(true);
        }


        if (Input.GetKeyDown("escape")&& pausa==false)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            pausa = true;
            Time.timeScale = 0;
            HudLimpio();
            Vi�eta.SetActive(true);
            HUDPausa();
            objetivo.gameObject.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            objetivo.gameObject.SetActive(true);
            pausa = false;
            Time.timeScale = 1;
            Vi�eta.SetActive(false);
            HUDPausaOFF();
        }
       
        if (Input.GetMouseButtonDown(0)&&Pistola.activeInHierarchy==false)
        {
            Ray ray = CamaraFPS.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("El rayo toc� al objeto: " + hit.collider.name);
            }
            if (hit.collider.name == ("Radio"))
            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                radio=objetoTocado.GetComponent<AudioSource>();
                if (radio.clip == true)
                {
                    radio.Play();
                    
                }
            }
            if (hit.collider.name == ("Llave"))
            {
                tienellave = true;
                GameObject key = GameObject.Find(hit.collider.name);
                key.SetActive(false);
                Llave.gameObject.SetActive(true);
            }
            if (hit.collider.name== ("Puerta"))
            {
                anim = hit.transform.GetComponentInParent<Animator>();
                GestorAudio.instancia.ReproducirSonido("Door");

                abierto = !abierto;

                anim.SetBool("Abierto", abierto);
            }
            if (hit.collider.name == ("Bidon"))
            {
                GameObject bidon = GameObject.Find(hit.collider.name);
                bidon.SetActive(false);
                Lintera.SetActive(false);
                tienelintera = false;
                Objetos[3].gameObject.SetActive(false);
                BidonJugador.SetActive(true);
                objetivo.text = ("Objetivo: Regresar al auto.");
                GestorAudio.instancia.ReproducirSonido("VZ_bidon");
                StartCoroutine(SubtitulosBidon());
                rapidezDesplazamiento = 2;
                tienegas = true;
                GestorAudio.instancia.PausarSonido("SN_viento");

            }
            if (hit.collider.name == ("Linterna"))
            {
                GameObject fl = GameObject.Find("LinternaDisplay");
                fl.SetActive(false);
                Lintera.SetActive(true);
                if (tienehacha==true)
                {
                    GameObject axe = GameObject.Find("Sostenedor de Arma");
                    axe.SetActive(false);
                    Objetos[4].gameObject.SetActive(false);
                }
                Objetos[0].gameObject.SetActive(true);
                Objetos[3].gameObject.SetActive(false);
                GestorAudio.instancia.ReproducirSonido("VZ_flashlight");
                StartCoroutine(SubtitulosLinterna());
                tienelintera = true;
            }
            if (hit.collider.name == ("NotaDisplay"))
            {
                Time.timeScale = 0;
                Nota.SetActive(true);
                GestorAudio.instancia.ReproducirSonido("VZ_note");
                StartCoroutine(SubtitulosNota());
                Time.timeScale = 1;
            }
            if(hit.collider.name == "NotaGuia")
            {
                Time.timeScale = 0;
                NotaGuia.SetActive(true);
                StartCoroutine(SubtitulosNota2());
                Time.timeScale = 1;
            }
            if (hit.collider.name == ("HachaDisplay"))
            {
                if (tienelintera == true)
                {
                    Objetos[0].gameObject.SetActive(false);
                    Lintera.SetActive(false);

                }
                GameObject axe = GameObject.Find(hit.collider.name);
                axe.SetActive(false);
                Objetos[3].gameObject.SetActive(false);
                Objetos[4].gameObject.SetActive(true);
                tienehacha = true;
                Hacha.SetActive(true);
            }
            if (hit.collider.name == ("BotonCuadro"))
            {
                GestorAudio.instancia.ReproducirSonido("BotonEaster");
                GameObject trampa = GameObject.Find("TrampaCuadro");
                Destroy(trampa);
                PostCuadro.SetActive(true);
            }
            if (hit.collider.name == ("TanqueGAS")&& tienegas==true)
            {
                Time.timeScale = 0;
                GameObject tanque = GameObject.Find("BidonJugador");
                Destroy(tanque);
                HudLimpio();
                Victoria.gameObject.SetActive(true);

            }
            if (hit.collider.name == ("BotonEaster1")|| hit.collider.name == ("BotonEaster2") || hit.collider.name == ("BotonEaster3"))
            {
                GestorAudio.instancia.ReproducirSonido("BotonEaster");
                contadorBotones++;
                if (contadorBotones == 1)
                {
                    ContadorEaster.text = "I";
                }
                if (contadorBotones == 2)
                {
                    ContadorEaster.text = "II";
                }
                if (contadorBotones == 3)
                {
                    ContadorEaster.text = "III";
                    GestorAudio.instancia.ReproducirSonido("Secreto");
                    CuadroSecreto.SetActive(false);
                    portal.SetActive(true);
                    StartCoroutine(SubtitulosSecreto());
                }
                GameObject boton = GameObject.Find(hit.collider.name);
                boton.SetActive(false);

            }
            if (hit.collider.name == ("B1"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD1.text = ("1");
            }
            if (hit.collider.name == ("B2"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD2.text = ("2");
            }
            if (hit.collider.name == ("B3"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD3.text = ("3");
            }
            if (hit.collider.name == ("B4"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD1.text = ("4");
            }
            if (hit.collider.name == ("B5"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD2.text = ("5");
            }
            if (hit.collider.name == ("B6"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD3.text = ("6");
            }
            if (hit.collider.name == ("B7"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD1.text = ("7");
            }
            if (hit.collider.name == ("B8"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD2.text = ("8");
            }
            if (hit.collider.name == ("B9"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_BPAD");
                DisplayD3.text = ("9");
            }
            if (hit.collider.name == ("Borrar"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_DeletePAD");
                DisplayD1.text = ("0");
                DisplayD2.text = ("0");
                DisplayD3.text = ("0");

            }
            if (hit.collider.name == ("Enter"))
            {
                GestorAudio.instancia.ReproducirSonido("SF_EnterPAD");
                if (DisplayD1.text=="4" && DisplayD2.text == "8" && DisplayD3.text == "3")
                {
                    rndf1.sharedMaterial = luzverde;
                    check1 = true;
                }
                else if (DisplayD1.text == "1" && DisplayD2.text == "5" && DisplayD3.text == "3")
                {
                    rndf2.sharedMaterial = luzverde;
                    check2 = true;

                }
            }
            if (hit.collider.name == ("BtnRnd"))
            {   
               int rnd1 = Random.Range(0, 5);
               dg1 = rnd1;
               int rnd2 = Random.Range(0, 5);
               dg2 = rnd2;
               int rnd3 = Random.Range(0, 5);
               dg3 = rnd3;
               int rnd4 = Random.Range(0, 5);
               dg4 = rnd4;
               int rnd5 = Random.Range(0, 5);
               dg5 = rnd5;
               int rnd6 = Random.Range(0, 5);
               dg6 = rnd6;
               ColRender[0].sharedMaterial = Colores[rnd1];
               ColRender[1].sharedMaterial = Colores[rnd2];
               ColRender[2].sharedMaterial = Colores[rnd3];
               ColRender[3].sharedMaterial = Colores[rnd4];
               ColRender[4].sharedMaterial = Colores[rnd5];
               ColRender[5].sharedMaterial = Colores[rnd6];
               StartCoroutine(TiempoColores());
                
            }
            if (hit.collider.name == ("BtnOK"))
            {
                if(ColRender[0].sharedMaterial== Colores[dg1]&& ColRender[1].sharedMaterial == Colores[dg2]&& ColRender[2].sharedMaterial == Colores[dg3] && ColRender[3].sharedMaterial == Colores[dg4] && ColRender[4].sharedMaterial == Colores[dg5] && ColRender[5].sharedMaterial == Colores[dg6])
                {
                    GestorAudio.instancia.ReproducirSonido("SF_EnterPAD");
                    Animator anim = PuertaLUZ.GetComponent<Animator>();
                    anim.SetTrigger("LightTrue");
                }
                else
                {
                    GestorAudio.instancia.ReproducirSonido("GAS_error");
                }
                
            }
            if (hit.collider.name == ("BtnEle"))
            {
                GestorAudio.instancia.ReproducirSonido("BotonEaster");
                
                Animator anim = Ascensor.GetComponent<Animator>();
                anim.Play("Animacion_Elevador");
                GestorAudio.instancia.ReproducirSonido("GAS_updw");
                contadorascensor++;
                if (contadorascensor == 1)
                {
                    StartCoroutine(SubsAscensor());
                }

            }
            if (hit.collider.name == ("BtnEleDW"))
            {
                GestorAudio.instancia.ReproducirSonido("BotonEaster");
                Animator anim = Ascensor.GetComponent<Animator>();
                    anim.Play("Animacion_ElevadorARRIBA");
                GestorAudio.instancia.ReproducirSonido("GAS_updw");

            }
            if (hit.collider.name == ("BateriaPick"))
            {
                BateriaPickUp.SetActive(false);
                Bateria.SetActive(true);
                tieneBateria = true;
                TriggerGas.SetActive(true);

            }

            // COMBINACION DE BOTONES DE LUZ.
            #region
            if (hit.collider.name == ("B1L1")) //L1
            {
                ColRender[0].sharedMaterial = Colores[0];
            }
            if (hit.collider.name == ("B2L1"))
            {
                ColRender[0].sharedMaterial = Colores[1];
            }
            if (hit.collider.name == ("B3L1"))
            {
                ColRender[0].sharedMaterial = Colores[2];
            }
            if (hit.collider.name == ("B4L1"))
            {
                ColRender[0].sharedMaterial = Colores[3];
            }
            if (hit.collider.name == ("B5L1"))
            {
                ColRender[0].sharedMaterial = Colores[4];
            }
            if (hit.collider.name == ("B1L2")) //L2
            {
                ColRender[1].sharedMaterial = Colores[0];
            }
            if (hit.collider.name == ("B2L2"))
            {
                ColRender[1].sharedMaterial = Colores[1];
            }
            if (hit.collider.name == ("B3L2"))
            {
                ColRender[1].sharedMaterial = Colores[2];
            }
            if (hit.collider.name == ("B4L2"))
            {
                ColRender[1].sharedMaterial = Colores[3];
            }
            if (hit.collider.name == ("B5L2"))
            {
                ColRender[1].sharedMaterial = Colores[4];
            }
            if (hit.collider.name == ("B1L3")) //L3
            {
                ColRender[2].sharedMaterial = Colores[0];
            }
            if (hit.collider.name == ("B2L3")) 
            {
                ColRender[2].sharedMaterial = Colores[1];
            }
            if (hit.collider.name == ("B3L3")) 
            {
                ColRender[2].sharedMaterial = Colores[2];
            }
            if (hit.collider.name == ("B4L3")) 
            {
                ColRender[2].sharedMaterial = Colores[3];
            }
            if (hit.collider.name == ("B5L3")) 
            {
                ColRender[2].sharedMaterial = Colores[4];
            }
            if (hit.collider.name == ("B1L4")) //L4
            {
                ColRender[3].sharedMaterial = Colores[0];
            }
            if (hit.collider.name == ("B2L4"))
            {
                ColRender[3].sharedMaterial = Colores[1];
            }
            if (hit.collider.name == ("B3L4"))
            {
                ColRender[3].sharedMaterial = Colores[2];
            }
            if (hit.collider.name == ("B4L4"))
            {
                ColRender[3].sharedMaterial = Colores[3];
            }
            if (hit.collider.name == ("B5L4"))
            {
                ColRender[3].sharedMaterial = Colores[4];
            }
            if (hit.collider.name == ("B1L5")) //L5
            {
                ColRender[4].sharedMaterial = Colores[0];
            }
            if (hit.collider.name == ("B2L5"))
            {
                ColRender[4].sharedMaterial = Colores[1];
            }
            if (hit.collider.name == ("B3L5"))
            {
                ColRender[4].sharedMaterial = Colores[2];
            }
            if (hit.collider.name == ("B4L5"))
            {
                ColRender[4].sharedMaterial = Colores[3];
            }
            if (hit.collider.name == ("B5L5"))
            {
                ColRender[4].sharedMaterial = Colores[4];
            }
            if (hit.collider.name == ("B1L6")) // L6
            {
                ColRender[5].sharedMaterial = Colores[0];
            }
            if (hit.collider.name == ("B2L6"))
            {
                ColRender[5].sharedMaterial = Colores[1];
            }
            if (hit.collider.name == ("B3L6"))
            {
                ColRender[5].sharedMaterial = Colores[2];
            }
            if (hit.collider.name == ("B4L6"))
            {
                ColRender[5].sharedMaterial = Colores[3];
            }
            if (hit.collider.name == ("B5L6"))
            {
                ColRender[5].sharedMaterial = Colores[4];
            }
            #endregion 


            if (hit.collider.name == ("C4Display")) // C4
            {
                GameObject c4display = GameObject.Find(hit.collider.name);
                Destroy(c4display);
                C4.SetActive(true);
                tienec4 = true;
            }
            if (hit.collider.name == ("PistolaDisplay"))
            {
                GameObject display = GameObject.Find(hit.collider.name);
                Destroy(display);
                Pistola.SetActive(true);
                tienepistola = true;
            }
        } //RAYCASTING NORMAL

        if(Input.GetMouseButtonDown(0) && Pistola.activeInHierarchy == true && balas>0) //RAYCASTING PISTOLA
        {
            Ray ray = CamaraFPS.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            Animator anim = Pistola.GetComponent<Animator>();
            anim.Play("AnimacionGunCon");
            flashpistola.Play();
            handgun.PlayOneShot(handgun.clip);
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 100)
            {
                GameObject bal= Instantiate(impacto, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(bal, 1f);
                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * 200);
                }
                if (hit.collider.tag == ("ROBATK"))
                {
                    GameObject robot = GameObject.Find("ROB ATK");
                    ROB_ATK codigo = robot.GetComponent<ROB_ATK>();
                    codigo.vida -= 25;
                    
                }
                if (hit.collider.name == ("BtnEleDW"))
                {
                    GestorAudio.instancia.ReproducirSonido("BotonEaster");
                    Animator anima = Ascensor.GetComponent<Animator>();
                    anima.Play("Animacion_ElevadorARRIBA");
                    GestorAudio.instancia.ReproducirSonido("GAS_updw");

                }
            }
            balas--;
        }
        if (Input.GetMouseButtonDown(0) && Pistola.activeInHierarchy == true && balas == 0) //RAYCASTING PISTOLA VACIA
        {
            Animator anim = Pistola.GetComponent<Animator>();
            anim.Play("GunConSinBalas");
           
        }
        if (Input.GetMouseButtonDown(1) && Pistola.activeInHierarchy == true) //RAYCASTING PISTOLA VACIA
        {
            Animator anim = Pistola.GetComponent<Animator>();
            anim.Play("ApuntadoGCon");

        }
        if (Input.GetKeyDown(KeyCode.R) && Pistola.activeInHierarchy==true && balas>0) //Recraga pistola.
        {
            //animacion de recarga.
            Animator anim = Pistola.GetComponent<Animator>();
            anim.Play("GunCon_Recarga");
            balas = 10;

        }
        if (Input.GetKeyDown(KeyCode.R) && Pistola.activeInHierarchy == true && balas == 0) //Recraga pistola.
        {
            //animacion de recarga.
            Animator anim = Pistola.GetComponent<Animator>();
            anim.Play("GunCon_RecargaCERO");
            balas = 10;

        }
        if (check1 == true && check2 == true)
        {

            Animator anim = PuertaLab.GetComponent<Animator>();
            anim.SetTrigger("CodeTrue");

        }

        if (Input.GetKey(KeyCode.C))
        {
            rapidezDesplazamiento = 2;
            transform.localScale = new Vector3(1f, 0.5f, 0.5f);
            EstadosJugador[0].gameObject.SetActive(false);
            EstadosJugador[1].gameObject.SetActive(false);
            EstadosJugador[2].gameObject.SetActive(false);
            EstadosJugador[3].gameObject.SetActive(true);
        } else
        {
            rapidezDesplazamiento = 4;
            transform.localScale = new Vector3(1f, 1.2f, 1f);
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && tienegas == false)
        {
            EstaCorriendo();
        }
        if (Input.GetKeyUp(KeyCode.LeftShift) && tienegas == false)
        {
            GestorAudio.instancia.PausarSonido("Corriendo");
        }
        if (Input.GetKey(KeyCode.LeftShift)&&tienegas==false)
        {
            rapidezDesplazamiento = 6;

        }
        else
        {
            rapidezDesplazamiento = 4;

        }
        if (Input.GetKeyDown(KeyCode.Alpha2)&&tienelintera==true)
        {
            if (Hacha.activeInHierarchy == true)
            {
                Hacha.SetActive(false);
            }
            if (C4.activeInHierarchy == true)
            {
                C4.SetActive(false);
            }
            Lintera.SetActive(true);
            Objetos[0].gameObject.SetActive(true);
            Objetos[1].gameObject.SetActive(false);
            Objetos[2].gameObject.SetActive(false);
            Objetos[3].gameObject.SetActive(false);
            Objetos[4].gameObject.SetActive(false);
            Objetos[5].gameObject.SetActive(false);


        }
        if (Input.GetKeyDown(KeyCode.Alpha1)&&tienelintera==true|| Input.GetKeyDown(KeyCode.Alpha1) && tienehacha == true || Input.GetKeyDown(KeyCode.Alpha1) && tienec4== true || Input.GetKeyDown(KeyCode.Alpha1) && tieneBateria == true || Input.GetKeyDown(KeyCode.Alpha1) && tienepistola == true)
        {
            Hacha.SetActive(false);
            Lintera.SetActive(false);
            C4.SetActive(false);
            Bateria.SetActive(false);
            Pistola.SetActive(false);
            Objetos[0].gameObject.SetActive(false);
            Objetos[1].gameObject.SetActive(false);
            Objetos[2].gameObject.SetActive(false);
            Objetos[3].gameObject.SetActive(true);
            Objetos[4].gameObject.SetActive(false);
            Objetos[5].gameObject.SetActive(false);
            Objetos[6].gameObject.SetActive(false);
            Objetos[7].gameObject.SetActive(false);



        }

        if (Input.GetKeyDown(KeyCode.F) && Lintera.activeInHierarchy==true && Hacha.activeInHierarchy == false && C4.activeInHierarchy==false) 
        {
            Objetos[0].gameObject.SetActive(false);
            Objetos[1].gameObject.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4) && tienehacha == true)
        {
            if (Lintera.activeInHierarchy == true)
            {
                Lintera.SetActive(false);
            }
            if (C4.activeInHierarchy == true)
            {
                C4.SetActive(false);
            }
            Hacha.SetActive(true);
            Objetos[0].gameObject.SetActive(false);
            Objetos[1].gameObject.SetActive(false);
            Objetos[2].gameObject.SetActive(false);
            Objetos[3].gameObject.SetActive(false);
            Objetos[4].gameObject.SetActive(true);
            Objetos[5].gameObject.SetActive(false);


        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            EstaCaminando();
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            EstaParado();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            EstaCaminando();
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            EstaParado();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            EstaCaminando();
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            EstaParado();
        }
        
        if (Input.GetKeyDown(KeyCode.D))
        {
            EstaCaminando();
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            EstaParado();
        }
        

        void EstaCaminando()
        {
            GestorAudio.instancia.PausarSonido("Corriendo");
            GestorAudio.instancia.ReproducirSonido("Caminando");
            EstadosJugador[0].gameObject.SetActive(false);
            EstadosJugador[1].gameObject.SetActive(true);
            EstadosJugador[2].gameObject.SetActive(false);
            EstadosJugador[3].gameObject.SetActive(false);
            
        }
        void EstaCorriendo()
        {
            GestorAudio.instancia.PausarSonido("Caminando");
            GestorAudio.instancia.ReproducirSonido("Corriendo");
            EstadosJugador[0].gameObject.SetActive(false);
            EstadosJugador[1].gameObject.SetActive(false);
            EstadosJugador[2].gameObject.SetActive(true);
            EstadosJugador[3].gameObject.SetActive(false);
        }
        void EstaParado()
        {
            GestorAudio.instancia.PausarSonido("Caminando");
            GestorAudio.instancia.PausarSonido("Corriendo");
            EstadosJugador[0].gameObject.SetActive(true);
            EstadosJugador[1].gameObject.SetActive(false);
            EstadosJugador[2].gameObject.SetActive(false);
            EstadosJugador[3].gameObject.SetActive(false);
        }
        




    }
    IEnumerator SubtitulosLinterna()
    {
        subs.text = ("Gracias a dios que siempre llevo una linterna conmigo.");
        yield return new WaitForSeconds(3.5f);
        subs.text = ("");
        
    }
    IEnumerator SubtitulosSecreto()
    {
        subs.text = ("Muy bien hecho, algo en la casa ha cambiado.");
        yield return new WaitForSeconds(4.6f);
        GestorAudio.instancia.ReproducirSonido("VZ_WWW");
        subs.text = ("Que fue eso?");
        yield return new WaitForSeconds(2f);
        subs.text = ("");
        yield return null;
    }
    IEnumerator SubtitulosBidon()
    {
        subs.text = ("Ok, hora de volver al auto.");
        yield return new WaitForSeconds(2.3f);
        subs.text = ("");
    }
    IEnumerator SubtitulosStart()
    {
        subs.text = ("Wow...�No es este el lugar perfecto para quedarse varado?");
        yield return new WaitForSeconds(3.6f);
        subs.text = ("");
    }
    IEnumerator SubtitulosNota()
    {
        subs.text = ("Una nota.");
        yield return new WaitForSeconds(3f);
        subs.text = ("");
        Nota.SetActive(false);
    }
    IEnumerator SubtitulosNota2()
    {
        GestorAudio.instancia.ReproducirSonido("VZ_note");
        subs.text = ("Otra nota.");
        yield return new WaitForSeconds(3f);
        subs.text = ("");
        NotaGuia.SetActive(false);
    }
    IEnumerator TiempoColores()
    {
        GestorAudio.instancia.ReproducirSonido("GAS_clock");
        yield return new WaitForSeconds(5f);
        ColRender[0].sharedMaterial = Colores[5];
        ColRender[1].sharedMaterial = Colores[5];
        ColRender[2].sharedMaterial = Colores[5];
        ColRender[3].sharedMaterial = Colores[5];
        ColRender[4].sharedMaterial = Colores[5];
        ColRender[5].sharedMaterial = Colores[5];
    }
    IEnumerator SubsAscensor()
    {
        subs.text = ("");
        yield return new WaitForSeconds(5f);
        GestorAudio.instancia.ReproducirSonido("GAS_elevator");
        subs.text = ("Disculpa si el ascensor se mueve mucho, es bastante viejo.");
        yield return new WaitForSeconds(4f);
        subs.text = ("");
    }

    void HudLimpio()
    {

        EstadosJugador[0].gameObject.SetActive(false);
        EstadosJugador[1].gameObject.SetActive(false);
        EstadosJugador[2].gameObject.SetActive(false);
        EstadosJugador[3].gameObject.SetActive(false);
        Objetos[0].gameObject.SetActive(false);
        Objetos[1].gameObject.SetActive(false);
        Objetos[2].gameObject.SetActive(false);
        Objetos[3].gameObject.SetActive(false);
        Objetos[4].gameObject.SetActive(false);
        Objetos[5].gameObject.SetActive(false);
        Objetos[6].gameObject.SetActive(false);
        objetivo.gameObject.SetActive(true);

    }
    void HUDPausa()
    {
        Pausa.gameObject.SetActive(true);
        P.gameObject.SetActive(true);
        GAS_pausa.gameObject.SetActive(true);
        Objetivo_Pausa.gameObject.SetActive(true);
        
    }
    void HUDPausaOFF()
    {
        Pausa.gameObject.SetActive(false);
        P.gameObject.SetActive(false);
        GAS_pausa.gameObject.SetActive(false);
        Objetivo_Pausa.gameObject.SetActive(false);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ROBATK"))
        {
            Time.timeScale = 0;
            HudLimpio();
            Derrota.gameObject.SetActive(true);
            GestorAudio.instancia.PausarSonido("SN_viento");
            GestorAudio.instancia.ReproducirSonido("ROB_jumpscare");
            ROB_ATK.Stop();
        }

    }
    
}
